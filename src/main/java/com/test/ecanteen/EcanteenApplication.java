package com.test.ecanteen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcanteenApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcanteenApplication.class, args);
	}

}
